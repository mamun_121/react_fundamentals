import React from 'react';
import SelectedLanguage from './SelectedLanguage'
import api from '../utils/api';
import RepoGrid from './RepoGrid';

class Popular extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            selectedLanguage: 'All',
            repos: null,
        }

        this.updateLanguage = this.updateLanguage.bind(this);
    }

    componentDidMount() {
        this.updateLanguage(this.state.selectedLanguage)
    }

    updateLanguage(lang) {
        this.setState(function () {
            return {
                selectedLanguage: lang,
                repos: null,
            }
        });

        api.fetctPopularRepos(lang)
            .then(function (repos) {
              //  console.log(repos)
               this.setState(function () {
                   return{
                       repos: repos
                   }
               })
            }.bind(this));
    }
    render() {

        return(
           <div>
               <SelectedLanguage
                   selectedLanguage={this.state.selectedLanguage}
                   onSelect={this.updateLanguage}
               />

               {!this.state.repos
                   ? <p>LOADING!</p>
                   : <RepoGrid repos={this.state.repos} />}

           </div>
        );
    }
}

export default Popular;