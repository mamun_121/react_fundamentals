/*
import React from 'react';
import PropTypes from 'prop-types';

class Try extends React.Component{
    render() {
        return(
          <div>
              <img
                  src={this.props.img}
                  alt='Avatar'
                  style={{width: 100, height:100}}
              />
              <h1>Name: {this.props.name}</h1>
              <h3>Username: {this.props.username}</h3>
          </div>
        );
    }

}


Try.propTypes = {
    img: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
}


export default Try;

// #1 Stateless Components
const Repos = React.createClass({
    render(){
        return (
            <div>
                <h3> User Repos </h3>
                <ul className="list-group">
                    {this.props.repos.map((repo, index) => {
                        return (
                            <li className="list-group-item" key={repo.name}>
                                <h4><a href={repo.html_url}>{repo.name}</a></h4>
                                <p>{repo.description}</p>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
})
// #1 Stateless Components
class Repos1 extends React.Component {
    render(){
        return (
            <div>
                <h3> User Repos </h3>
                <ul className="list-group">
                    {this.props.repos.map((repo, index) => {
                        return (
                            <li className="list-group-item" key={repo.name}>
                                <h4><a href={repo.html_url}>{repo.name}</a></h4>
                                <p>{repo.description}</p>
                            </li>
                        )
                    })}
                </ul>
            </div>
        )
    }
}

// #2 Stateless Functional Components
const Repos2 = ({repos}) => {
    return (
        <div>
            <h3> User Repos </h3>
            <ul className="list-group">
                {repos.map((repo, index) => {
                    return (
                        <li className="list-group-item" key={repo.name}>
                            <h4><a href={repo.html_url}>{repo.name}</a></h4>
                            <p>{repo.description}</p>
                        </li>
                    )
                })}
            </ul>
        </div>
    )
}
*/

import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

function BasicExample() {
    return (
        <Router>
            <div>
                <ul>
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/about">About</Link>
                    </li>
                    <li>
                        <Link to="/topics">Topics</Link>
                    </li>
                </ul>

                <hr />

                <Route exact path="/" component={Home} />
                <Route path="/about" component={About} />
                <Route path="/topics" component={Topics} />
            </div>
        </Router>
    );
}

function Home() {
    return (
        <div>
            <h2>Home</h2>
        </div>
    );
}

function About() {
    return (
        <div>
            <h2>About</h2>
        </div>
    );
}

function Topics({ match }) {
    return (
        <div>
            <h2>Topics</h2>
            <ul>
                <li>
                    <Link to={`${match.url}/rendering`}>Rendering with React</Link>
                </li>
                <li>
                    <Link to={`${match.url}/components`}>Components</Link>
                </li>
                <li>
                    <Link to={`${match.url}/props-v-state`}>Props v. State</Link>
                </li>
            </ul>

            <Route path={`${match.path}/:topicId`} component={Topic} />
            <Route
                exact
                path={match.path}
                render={() => <h3>Please select a topic.</h3>}
            />
        </div>
    );
}

function Topic({ match }) {
    return (
        <div>
            <h3>{match.params.topicId}</h3>
        </div>
    );
}

export default BasicExample;

