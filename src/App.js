import React from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import './App.css';
import Popular from './components/Popular';
import Nav from './components/Nav';
import Home from './components/Home';
import Battle from './components/Battle';

function App() {
  return (
      <Router>
          <div className='container'>
              <Nav/>
              <Switch>
                  <Route path='/popular' component={Popular}/>
                  <Route exact path='/' component={Home} />
                  <Route exact path='/battle' component={Battle} />
                  <Route render={function () {
                      return <p> Not Found </p>
                  }}
                  />
              </Switch>

          </div>
      </Router>

  );
}

export default App;
