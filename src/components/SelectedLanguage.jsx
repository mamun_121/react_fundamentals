import React from 'react';
import PropTypes from 'prop-types';


function SelectedLanguage(props) {
    const languages = ['All', 'JavaScript', 'Ruby', 'Java', 'CSS', 'Python'];
    return(
        <ul className='languages'>
            {languages.map(function (lang) {
                return(
                    <li
                        style={lang === props.selectedLanguage ? {color: '#d0021b'}: null}
                        key={lang}
                        onClick={props.onSelect.bind(null, lang)}>
                        {lang}
                    </li>
                );
            }, this)}
        </ul>
    );
}

SelectedLanguage.propTypes = {
    selectedLanguage: PropTypes.string.isRequired,
    onSelect: PropTypes.func.isRequired,
}

export default SelectedLanguage;